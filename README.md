# An integrated dynamic facility layout and job shop scheduling problem: A hybrid 3 NSGA-II and local search algorithm #

### What is this repository for? ###

This is a repository for the input data of all random solutions generated in this paper. The interested readers can use the following data for any future comparison. 

- Instance 1:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance1.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance1.xlsx

- Instance 2:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance2.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance2.xlsx

- Instance 3:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance3.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance3.xlsx

- Instance 4:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance4.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance4.xlsx

- Instance 5:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance5.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance5.xlsx

- Instance 6:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance6.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance6.xlsx

- Instance 7:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance7.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance7.xlsx

- Instance 8:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance8.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance8.xlsx

- Instance 9:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance9.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance9.xlsx

- Instance 10:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance10.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance10.xlsx

- Instance 11:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance11.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance11.xlsx

- Instance 12:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance12.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance12.xlsx

- Instance 13:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance13.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance13.xlsx

- Instance 14:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance14.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance14.xlsx

- Instance 15:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance15.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance15.xlsx

- Instance 16:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance16.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance16.xlsx

- Instance 17:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance17.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance17.xlsx

- Instance 18:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance18.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance18.xlsx

- Instance 19:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance19.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance19.xlsx

- Instance 20:
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance20.txt
https://bitbucket.org/Pro_Data/instances_2/downloads/Instance20.xlsx
